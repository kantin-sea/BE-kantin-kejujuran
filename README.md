### Setup Environment

1. Clone repository

```
git clone https://gitlab.com/kantin-sea/BE-kantin-kejujuran.git
```

2. Get into project folder

```
cd BE-kantin-kejujuran
``` 

3. Create a Python virtual environment

```
python3 -m venv venv
virtualenv venv
```

4. Activate the virtual environment
```
venv\Scripts\activate (Windows)
source ../venv/bin/activate (Ubuntu)
```

5. Install required packages

```
pip install -r requirements.txt
```

6. Do database migrations

```
python manage.py migrate
```

7. Run in localhost

```
python manage.py runserver
```

### Links

- BackEnd URL: https://kantin-kejujuran-sea.herokuapp.com/