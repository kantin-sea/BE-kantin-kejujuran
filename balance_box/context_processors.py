from .models import Canteen

def settings(request):
    return {'settings': Canteen.load()}