from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Canteen

@api_view(['GET'])
def get_balance(request):
    canteen_obj = Canteen.load()
    saldo = canteen_obj.get_balance()
    return Response({'saldo':saldo},status=200)

@api_view(['PUT'])
def top_up(request):
    nominal = request.data['nominal']
    canteen_obj = Canteen.load()
    canteen_obj.top_up(nominal)
    return Response({'status':'success', 'message':'Top Up Success'},status=200)

@api_view(['PUT'])
def withdraw(request):
    try:
        nominal = request.data['nominal']
        canteen_obj = Canteen.load()
        canteen_obj.withdraw(nominal)
        return Response({'status':'success', 'message':'Withdraw Success'},status=200)
    except:
        return Response({'status':'Failed', 'message':'Cannot withdraw more than current balance'},status=400)
