from django.apps import AppConfig


class BalanceBoxConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'balance_box'
