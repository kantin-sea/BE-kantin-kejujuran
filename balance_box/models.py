from django.db import models
# Create your models here.

from django.db import models

class SingletonModel(models.Model):

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj

class Canteen(SingletonModel):
    balance = models.IntegerField(default=0)

    def __str__(self):
        return 'Kantin Kejujuran'

    def get_balance(self):
        return self.balance

    def top_up(self, nominal):
        self.balance += int(nominal)
        super().save()

    def withdraw(self, nominal):
        if int(nominal) <= self.balance:
            self.balance -= int(nominal)
            super().save()
        else:
            raise Exception("Cannot withdraw more than current balance")


