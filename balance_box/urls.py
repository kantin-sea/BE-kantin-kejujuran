from django.urls import path
from .views import top_up, withdraw, get_balance

urlpatterns = [
    path('balance/', get_balance),
    path('top-up/', top_up),
    path('withdraw/', withdraw),
]
