from django.urls import path
from .views import ProdukList, ProdukDetail, ProdukSort

urlpatterns = [
    path('produk/', ProdukList.as_view()),
    path('sortProduk/<str:sortBy>', ProdukSort.as_view()),
    path('beli/<int:pk>', ProdukDetail.as_view()),
]
