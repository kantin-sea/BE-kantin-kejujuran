from django.db import models

# Create your models here.
class Produk(models.Model):
    name = models.CharField(max_length = 250)
    price = models.IntegerField()
    desc = models.TextField()
    image = models.ImageField(upload_to="produk/")
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
