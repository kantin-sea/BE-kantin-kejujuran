from rest_framework.exceptions import ValidationError
from rest_framework import generics
from .serializers import ProdukSerializer
from .models import Produk
from rest_framework.response import Response

class ProdukList(generics.ListCreateAPIView):
    serializer_class = ProdukSerializer

    def get_queryset(self):
        return Produk.objects.all()

    def create(self, request,  *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            return Response({'status':'success','message':'Produk berhasil ditambahkan', 'produk':serializer.data},
                             status=201)
        return Response(serializer.errors, status=400)

class ProdukSort(generics.ListAPIView):
    serializer_class = ProdukSerializer

    def get_queryset(self):
        sortBy = self.kwargs['sortBy']
        if sortBy == 'date':
            return Produk.objects.order_by('-timestamp')
        elif sortBy == 'name':
            return Produk.objects.order_by('name')
        else:
            return Produk.objects.all()


class ProdukDetail(generics.DestroyAPIView):
    serializer_class = ProdukSerializer
    def get_queryset(self):
        return Produk.objects.all()

    def destroy(self, request,  *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({'status':'success','message':'Produk berhasil dibeli'})

